package at.ssl.dao;

import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.ShoppingListResource;

public interface IShoppingListDAO {

	public ShoppingListResource createShoppingList(ShoppingListResource sl) throws IDNotFoundException;

	public ShoppingListCollection getAllShoppingLists();

	public void deleteShoppingList(int idShoppingList)
			throws IDNotFoundException;
	
	public void modifyShoppingList(ShoppingListResource sl) throws IDNotFoundException;
	
	public ShoppingListResource getShoppingListById(int idShoppingList) throws IDNotFoundException;
	
	public ShoppingListCollection getOwnedShoppingLists(int iduser) throws IDNotFoundException;

}
