package at.ssl.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.ShoppingListResource;

public class ShoppingListDAO implements IShoppingListDAO {

	private IDBConnector db;
	private PreparedStatement ps;
	private static Logger log = LogManager.getLogger(ShoppingListDAO.class
			.getName());

	public ShoppingListDAO() {
		db = new DBConnector();
	}

	@Override
	public ShoppingListResource createShoppingList(ShoppingListResource sl)
			throws IDNotFoundException {
		log.info("execute createShoppingList()");

		try {
			ps = db.connect().prepareStatement(SQLQueries.CREATE_SHOPPING_LIST,
					Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, sl.getName());
			ps.setString(2, sl.getPicture());
			ps.setString(3, sl.getComment());
			ps.setInt(4, sl.getOwner());

			int result = ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			if (rs.next())
				sl.setIdShoppingList(rs.getInt(1));

			if (result != 0) {
				ps.close();
				db.close();
				return sl;
			}

		} catch (SQLException e) {
			if (e.toString().contains("foreign key constraint fails")) {

				throw new IDNotFoundException("Owner does not exist error!",
						"The User with id: " + sl.getOwner()
								+ " is not in the System", 40412);
			}

			log.error(e.getMessage());
		}

		log.info("method createShoppingList with update finished");
		log.info("ShoppingList: " + sl + " " + sl.getName());
		return null;
	}

	@Override
	public ShoppingListCollection getAllShoppingLists() {
		log.info("execute getAllShoppingLists()");

		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_ALL_SHOPPING_LISTS);

			ResultSet rs = ps.executeQuery();

			ShoppingListCollection sCollection = new ShoppingListCollection();

			while (rs.next()) {
				ShoppingListResource sl = new ShoppingListResource();

				sl.setIdShoppingList(rs.getInt("idshopping_list"));
				sl.setName(rs.getString("name"));
				sl.setPicture(rs.getString("picture"));
				sl.setComment(rs.getString("comment"));
				sl.setOwner(rs.getInt("owner"));
				sl.setCreated(rs.getTimestamp("created").getTime());
				sl.setUpdated(rs.getTimestamp("updated").getTime());

				sCollection.addShoppingListResource(sl);
			}

			ps.close();
			db.close();

			if (sCollection.size() > 0)
				return sCollection;

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		return null;
	}

	@Override
	public void deleteShoppingList(int idShoppingList)
			throws IDNotFoundException {
		log.info("execute deleteUser(" + idShoppingList + ")");

		try {
			ps = db.connect().prepareStatement(
					SQLQueries.DELETE_SHOPPING_LIST_BY_ID);

			ps.setInt(1, idShoppingList);

			if (ps.executeUpdate() == 0) {
				ps.close();
				db.close();

				throw new IDNotFoundException("ID Not Found Error",
						"No entry found that matches id: " + idShoppingList
								+ "! No Shopping List has been deleted", 40409);
			}

			ps.close();
			db.close();

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

	}

	@Override
	public void modifyShoppingList(ShoppingListResource sl)
			throws IDNotFoundException {
		log.info("execute modifyShoppingList(" + sl.getIdShoppingList() + ")");

		try {
			ps = db.connect().prepareStatement(SQLQueries.UPDATE_SHOPPING_LIST);

			ps.setString(1, sl.getName());
			ps.setString(2, sl.getPicture());
			ps.setString(3, sl.getComment());
			ps.setInt(4, sl.getOwner());

			ps.setInt(5, sl.getIdShoppingList());

			if (ps.executeUpdate() < 1) {
				ps.close();
				db.close();

				throw new IDNotFoundException("Object not found error",
						"No shopping list found with id: "
								+ sl.getIdShoppingList()
								+ "! Shopping List could not be modified!",
						40410);
			}

			ps.close();
			db.close();

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

	}

	@Override
	public ShoppingListResource getShoppingListById(int idShoppingList)
			throws IDNotFoundException {
		log.info("execute getShoppingListById(" + idShoppingList + ")");

		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_SHOPPING_LIST_BY_ID);

			ps.setInt(1, idShoppingList);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				ShoppingListResource sl = new ShoppingListResource();

				sl.setIdShoppingList(rs.getInt("idshopping_list"));
				sl.setName(rs.getString("name"));
				sl.setPicture(rs.getString("picture"));
				sl.setComment(rs.getString("comment"));
				sl.setOwner(rs.getInt("owner"));
				sl.setCreated(rs.getTimestamp("created").getTime());
				sl.setUpdated(rs.getTimestamp("updated").getTime());

				Date date = new Date(sl.getCreated());
				java.util.Date newDate = new java.util.Date(date.getTime());
				log.info("Time in ms: "+sl.getCreated()+" SQL Date: "+date.toString()+" long: "+date.getTime() + "new long: "+newDate.getTime() + newDate.toString() );
				
				
				ps.close();
				db.close();

				return sl;
			} else {
				ps.close();
				db.close();

				throw new IDNotFoundException("ID not found error!",
						"shopping list id not found in Database!", 40411);
			}

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		log.debug("No Result, No error!");
		return null;
	}

	@Override
	public ShoppingListCollection getOwnedShoppingLists(int iduser)
			throws IDNotFoundException {
		log.info("execute getOwnedShoppingLists(" + iduser + ")");
		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_OWNED_SHOPPINGLISTS_BY_USER_ID);

			ps.setInt(1, iduser);

			ResultSet rs = ps.executeQuery();

			ShoppingListCollection sCollection = new ShoppingListCollection();

			while (rs.next()) {
				ShoppingListResource sl = new ShoppingListResource();

				sl.setIdShoppingList(rs.getInt("idshopping_list"));
				sl.setName(rs.getString("name"));
				sl.setPicture(rs.getString("picture"));
				sl.setComment(rs.getString("comment"));
				sl.setOwner(rs.getInt("owner"));
				sl.setCreated(rs.getTimestamp("created").getTime());
				sl.setUpdated(rs.getTimestamp("updated").getTime());

				sCollection.addShoppingListResource(sl);
			}

			ps.close();
			db.close();

			if (sCollection.size() > 0)
				return sCollection;
			else {
				throw new IDNotFoundException("User Owns no ShoppingList", "The User with the id: "+iduser+" owns no shopping lists", 40429);
			}
		} catch (SQLException e) {
			log.error(e.getMessage());

		}

		return null;
	}

}
