package at.ssl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ssl.exception.DuplicateObjectException;
import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.InviteResource;
import at.ssl.resource.PasswordResource;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.ShoppingListResource;
import at.ssl.resource.UserCollection;
import at.ssl.resource.UserResource;

public class UserDAO implements IUserDAO {

	private IDBConnector db;
	private PreparedStatement ps;
	private static Logger log = LogManager.getLogger(DBConnector.class
			.getName());

	public UserDAO() {
		db = new DBConnector();
	}

	@Override
	public UserResource createUser(UserResource u)
			throws DuplicateObjectException {
		log.info("execute createUser()");

		try {
			ps = db.connect().prepareStatement(SQLQueries.CREATE_USER,
					Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, u.getMail());
			ps.setString(2, u.getUsername());
			ps.setString(3, u.getAvatar());
			ps.setString(4, u.getPassword());
			ps.setBoolean(5, true);
			int result = ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			if (rs.next()) {
				u.setActivated(true);
				u.setIduser(rs.getInt(1));
			}

			if (result != 0) {
				ps.close();
				db.close();
				return u;
			}

			ps.close();
			db.close();

		} catch (SQLException e) {
			if (e.toString().contains("Duplicate entry"))
				throw new DuplicateObjectException("Object duplicate error",
						"eMail already exists in system: " + u.getMail()
								+ "! User could not be modified!", 40406);

		}

		return null;

	}

	@Override
	public void deleteUser(int iduser) throws IDNotFoundException {
		log.info("execute deleteUser()");

		try {
			ps = db.connect().prepareStatement(SQLQueries.DELETE_USER_BY_ID);

			ps.setInt(1, iduser);

			if (ps.executeUpdate() == 0) {
				ps.close();
				db.close();

				throw new IDNotFoundException("ID Not Found Error",
						"No entry found that matches id: " + iduser
								+ "! No User has been deleted", 40402);

			}

			ps.close();
			db.close();

		} catch (SQLException e) {

			log.error(e.getMessage());
		}
	}

	@Override
	public void modifyUser(UserResource u) throws IDNotFoundException,
			DuplicateObjectException {
		log.info("execute modifyUser()");

		try {

			ps = db.connect().prepareStatement(SQLQueries.UPDATE_USER);

			ps.setString(1, u.getMail());
			ps.setString(2, u.getUsername());
			ps.setString(3, u.getAvatar());
			ps.setInt(4, u.getIduser());

			if (ps.executeUpdate() < 1) {
				ps.close();
				db.close();

				throw new IDNotFoundException("Object not found error",
						"No user found with id: " + u.getIduser()
								+ "! User could not be modified!", 40403);
			}

			ps.close();
			db.close();

		} catch (SQLException e) {

			if (e.toString().contains("Duplicate entry"))
				throw new DuplicateObjectException("Object duplicate error",
						"eMail already exists in system: " + u.getMail()
								+ "! User could not be modified!", 40406);
			else
				e.printStackTrace();

		}

	}

	@Override
	public UserCollection getAllUsers() {
		log.info("execute getAllUsers()");

		try {
			ps = db.connect().prepareStatement(SQLQueries.SELECT_ALL_USERS);
			ResultSet rs = ps.executeQuery();

			UserCollection uCollection = new UserCollection();
			while (rs.next()) {
				UserResource user = new UserResource();

				user.setIduser(rs.getInt("iduser"));
				user.setMail(rs.getString("mail"));
				user.setUsername(rs.getString("username"));
				user.setActivated(rs.getBoolean("activated"));
				user.setAvatar(rs.getString("avatar"));
				user.setPassword(rs.getString("password"));

				uCollection.addUserResource(user);
			}

			ps.close();
			db.close();
			if (uCollection.size() > 0)
				return uCollection;

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		return null;
	}

	@Override
	public UserResource getUserById(int iduser) throws IDNotFoundException {
		log.info("execute getUserById(int " + iduser + ")");

		try {
			ps = db.connect().prepareStatement(SQLQueries.SELECT_USER_BY_ID);
			ps.setInt(1, iduser);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				UserResource user = new UserResource();

				user.setIduser(rs.getInt("iduser"));
				user.setMail(rs.getString("mail"));
				user.setUsername(rs.getString("username"));
				user.setActivated(rs.getBoolean("activated"));
				user.setAvatar(rs.getString("avatar"));
				user.setPassword(rs.getString("password"));

				ps.close();
				db.close();

				return user;
			} else {
				ps.close();
				db.close();
				throw new IDNotFoundException("ID not found error",
						"user id not found in Database", 40401);
			}

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		return null;
	}

	@Override
	public void addInvitedUser(InviteResource i)
			throws DuplicateObjectException {
		log.info("execute InviteUser()");

		try {
			ps = db.connect().prepareStatement(SQLQueries.CREATE_INVITE_USER);

			ps.setString(1, i.getInviteeMail());
			ps.setString(2, i.getInviteeUsername());

			if (ps.executeUpdate() < 1) {
				ps.close();
				db.close();

			}

			ps.close();
			db.close();

		} catch (SQLException e) {
			if (e.toString().contains("Duplicate entry"))
				throw new DuplicateObjectException("Email exists error!",
						"The eMail: " + i.getInviteeMail()
								+ " is already in the System", 40404);
			else
				log.error(e.getMessage());
		}

	}

	@Override
	public void changePassword(PasswordResource p) throws IDNotFoundException {

		try {
			ps = db.connect().prepareStatement(SQLQueries.UPDATE_USER_PASSWORD);

			ps.setString(1, p.getNewPassword());
			ps.setInt(2, p.getIduser());

			if (ps.executeUpdate() < 1) {
				ps.close();
				db.close();
				throw new IDNotFoundException(
						"User not found error!",
						"User with id: "
								+ p.getIduser()
								+ " was not found! Password could not be changed",
						40405);
			}

			ps.close();
			db.close();

		} catch (SQLException e) {

			log.error(e.getMessage());
		}

	}

	@Override
	public String getPasswordByUser(int iduser) throws IDNotFoundException {

		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_GET_PASSWORD_BY_USER);

			ps.setInt(1, iduser);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				ps.close();
				db.close();
				return rs.getString("password");
			} else {
				ps.close();
				db.close();
				throw new IDNotFoundException(
						"User not found error",
						"User with id: " + iduser
								+ " not found! Password could not be retrieved",
						40407);
			}

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		return null;
	}

	@Override
	public void subscribeUserToShoppingList(int iduser, int idShoppingList)
			throws IDNotFoundException {
		log.info("execute subscribeUserToShoppingList(" + iduser + ", "
				+ idShoppingList + ")");
		try {
			ps = db.connect().prepareStatement(
					SQLQueries.INSERT_SUBSCRIBE_SHOPPING_LIST);

			ps.setInt(1, iduser);
			ps.setInt(2, idShoppingList);

			ps.executeUpdate();

			ps.close();
			db.close();
		} catch (SQLException e) {

			if (e.toString().contains("foreign key")) {
				log.warn(e.getMessage());
				throw new IDNotFoundException("Email exists error!",
						"The User with id: " + iduser
								+ "or the shoppinglist with id "
								+ idShoppingList + " is not in the System!",
						40423);
			} else
				log.error(e.getMessage());
		}

	}

	@Override
	public boolean isUserSubscribedToShoppingList(int iduser, int idShoppingList)
			throws IDNotFoundException {

		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_USER_IS_SUBSCRIBED);

			ps.setInt(1, iduser);
			ps.setInt(2, idShoppingList);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				ps.close();
				db.close();
				return rs.getBoolean("is_subscribed");
			}

			ps.close();
			db.close();

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		return false;
	}

	@Override
	public void unsubscribeUserFromShoppingList(int iduser, int idShoppingList)
			throws IDNotFoundException {
		log.info("execute unsubscribeUserFromShoppingList(" + iduser + ", "
				+ idShoppingList + ")");
		try {
			ps = db.connect().prepareStatement(
					SQLQueries.DELETE_SUBSCRIBED_USER);

			ps.setInt(1, iduser);
			ps.setInt(2, idShoppingList);

			if (ps.executeUpdate() == 0) {
				ps.close();
				db.close();

				throw new IDNotFoundException("ID Not Found Error",
						"No entry found that matches id: " + iduser
								+ "! No User has been deleted", 40424);
			}

			ps.close();
			db.close();
		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		log.info("Deletion Completed!");

	}

	@Override
	public ShoppingListCollection getShoppingListByUser(int iduser)
			throws IDNotFoundException {
		log.info("execute getShoppingListByUser("+iduser+")");
		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_SHOPPING_LISTS_BY_USER);

			ps.setInt(1, iduser);

			ResultSet rs = ps.executeQuery();

			ShoppingListCollection sCollection = new ShoppingListCollection();

			while (rs.next()) {
				ShoppingListResource sl = new ShoppingListResource();

				sl.setIdShoppingList(rs.getInt("idshopping_list"));
				sl.setName(rs.getString("name"));
				sl.setPicture(rs.getString("picture"));
				sl.setComment(rs.getString("comment"));
				sl.setOwner(rs.getInt("owner"));
				sl.setCreated(rs.getTimestamp("created").getTime());
				sl.setUpdated(rs.getTimestamp("updated").getTime());

				sCollection.addShoppingListResource(sl);
			}

			ps.close();
			db.close();

			if (sCollection.size() > 0)
				return sCollection;
		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		return null;
	}

	@Override
	public UserResource getUserByEmail(String eMail) throws IDNotFoundException {
		try {
			ps = db.connect().prepareStatement(SQLQueries.SELECT_USER_BY_EMAIL);
			
			ps.setString(1, eMail);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				UserResource user = new UserResource();
				user.setIduser(rs.getInt("iduser"));
				user.setMail(rs.getString("mail"));
				user.setUsername(rs.getString("username"));
				user.setActivated(rs.getBoolean("activated"));
				user.setAvatar(rs.getString("avatar"));
				user.setPassword(rs.getString("password"));

				ps.close();
				db.close();
				
				return user;
			} else {
				throw new IDNotFoundException("No User Found",
						"No User found that matches mail: " + eMail
								+ "!", 40444);
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
		
		
		return null;
	}

}
