package at.ssl.dao;

import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ItemCollection;
import at.ssl.resource.ItemResource;

public interface IItemDAO {

	public ItemResource createItemResource(ItemResource i) throws IDNotFoundException;
	
	public ItemCollection getAllItemsOfShoppingList(int idShoppingList);
	
	public void modifyItemResource(ItemResource i) throws IDNotFoundException;

	public void deleteItemResource(int idItem) throws IDNotFoundException;
	
	public ItemResource getItemById(int idItem) throws IDNotFoundException;

	void setItemAsDone(int iditem, boolean isDone) throws IDNotFoundException;
}
