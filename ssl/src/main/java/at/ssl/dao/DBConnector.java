package at.ssl.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DBConnector implements IDBConnector {
	private Connection connection;

	private static Logger log = LogManager.getLogger(DBConnector.class.getName());

	public DBConnector() {
	}

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/ssl";

	private static final String USER = "root";
	private static final String PASSWORD = "";

	@Override
	public Connection connect() {

		log.info("Start DB connection attempt!");

		if (connection == null) {

			try {
				Class.forName(JDBC_DRIVER);

				connection = DriverManager
						.getConnection(DB_URL, USER, PASSWORD);
				log.info("Connection Successfully established!");
			} catch (ClassNotFoundException e) {
				log.error(e.getMessage());
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}
		return connection;
	}

	@Override
	public void close() {
		log.info("Close DB connection!");

		if (connection != null) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException e) {
				log.error(e.getMessage());
			}

		}

	}

}
