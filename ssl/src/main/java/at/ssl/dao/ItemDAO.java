package at.ssl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ItemCollection;
import at.ssl.resource.ItemResource;

public class ItemDAO implements IItemDAO {

	private IDBConnector db;
	private PreparedStatement ps;

	private static Logger log = LogManager.getLogger(ItemDAO.class.getName());

	public ItemDAO() {
		db = new DBConnector();
	}

	@Override
	public ItemResource createItemResource(ItemResource i)
			throws IDNotFoundException {
		log.info("execute createItemResource(" + i.getName() + ")");

		try {
			ps = db.connect().prepareStatement(SQLQueries.CREATE_ITEM,
					Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, i.getPicture());
			ps.setDouble(2, i.getPrice());
			ps.setString(3, i.getName());
			ps.setString(4, i.getComment());
			ps.setInt(5, i.getPriority());
			ps.setInt(6, i.getShopping_list_idshopping_list());
			ps.setInt(7, i.getOwner());
			ps.setBoolean(8, i.isDone());

			int result = ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			if (rs.next())
				i.setIdItem(rs.getInt(1));

			if (result != 0) {
				ps.close();
				db.close();

				return i;
			}

		} catch (SQLException e) {
			if (e.toString().contains("foreign key constraint fails")) {
				log.warn(e.getMessage());
				throw new IDNotFoundException(
						"Owner or Shopping List not found error!",
						"The user with id: " + i.getOwner()
								+ " or Shopping List with id: "
								+ i.getShopping_list_idshopping_list()
								+ " are not in the System", 40430);
			}
		}

		log.info("No Item Created!");
		return null;
	}

	@Override
	public ItemCollection getAllItemsOfShoppingList(int idShoppingList) {
		log.info("execute getAllItemsOfShoppingList(" + idShoppingList + ")");
		try {
			ps = db.connect().prepareStatement(
					SQLQueries.SELECT_ALL_ITEMS_OF_LIST);

			ps.setInt(1, idShoppingList);

			ResultSet rs = ps.executeQuery();

			ItemCollection iCollection = new ItemCollection();

			while (rs.next()) {
				ItemResource i = new ItemResource();

				i.setIdItem(rs.getInt("iditem"));
				i.setPicture(rs.getString("picture"));
				i.setPrice(rs.getDouble("price"));
				i.setName(rs.getString("name"));
				i.setComment(rs.getString("comment"));
				i.setPriority(rs.getInt("priority"));
				i.setShopping_list_idshopping_list(rs
						.getInt("shopping_list_idshopping_list"));
				i.setOwner(rs.getInt("owner"));
				i.setCreated(rs.getTimestamp("created").getTime());
				i.setUpdated(rs.getTimestamp("updated").getTime());
				i.setDone(rs.getBoolean("done"));

				iCollection.addItemResource(i);

				log.info("count sets of items");

			}

			log.info("Collection Size: " + iCollection.size());
			ps.close();
			db.close();

			if (iCollection.size() > 0)
				return iCollection;

		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		log.info("No Lines Found");
		return null;
	}

	@Override
	public void modifyItemResource(ItemResource i) throws IDNotFoundException {
		log.info("execute modifyItemResource(" + i.getIdItem() + ")");

		try {
			ps = db.connect().prepareStatement(SQLQueries.UPDATE_ITEM);

			ps.setString(1, i.getPicture());
			ps.setDouble(2, i.getPrice());
			ps.setString(3, i.getName());
			ps.setString(4, i.getComment());
			ps.setInt(5, i.getPriority());
			ps.setInt(6, i.getShopping_list_idshopping_list());
			ps.setInt(7, i.getOwner());
			ps.setBoolean(8, i.isDone());
			ps.setInt(9, i.getIdItem());

			if (ps.executeUpdate() < 1) {
				ps.close();
				db.close();

				throw new IDNotFoundException(
						"Object not found error",
						"No shopping lsit item found with id: " + i.getIdItem()
								+ "! Shopping List Item could not be modified!",
						40431);
			}

			ps.close();
			db.close();

		} catch (SQLException e) {
			log.error(e.getMessage());
		}
	}

	@Override
	public void deleteItemResource(int idItem) throws IDNotFoundException {
		log.info("execute deleteItemResource(" + idItem + ")");

		try {
			ps = db.connect().prepareStatement(SQLQueries.DELETE_ITEM);

			ps.setInt(1, idItem);

			if (ps.executeUpdate() == 0) {
				ps.close();
				db.close();

				throw new IDNotFoundException("ID Not Found Error",
						"No entry found that matches id: " + idItem
								+ "! No Shopping List Item has been deleted",
						40432);
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		}

	}

	@Override
	public ItemResource getItemById(int idItem) throws IDNotFoundException {
		log.info("execute getItemById(" + idItem + ")");
		try {
			ps = db.connect().prepareStatement(SQLQueries.SELECT_ITEM_BY_ID);

			ps.setInt(1, idItem);

			ResultSet rs = ps.executeQuery();

			ItemResource i = new ItemResource();

			if (rs.next()) {

				i.setIdItem(rs.getInt("iditem"));
				i.setPicture(rs.getString("picture"));
				i.setPrice(rs.getDouble("price"));
				i.setName(rs.getString("name"));
				i.setComment(rs.getString("comment"));
				i.setPriority(rs.getInt("priority"));
				i.setShopping_list_idshopping_list(rs
						.getInt("shopping_list_idshopping_list"));
				i.setOwner(rs.getInt("owner"));
				i.setCreated(rs.getTimestamp("created").getTime());
				i.setUpdated(rs.getTimestamp("updated").getTime());
				i.setDone(rs.getBoolean("done"));

				ps.close();
				db.close();

				return i;

			} else {
				ps.close();
				db.close();

				throw new IDNotFoundException("ID not found error!",
						"shopping list item id not found in Database", 40433);
			}
		} catch (SQLException e) {
			log.error(e.getMessage());
		}

		log.debug("No Result, No error!");
		return null;
	}

	@Override
	public void setItemAsDone(int iditem, boolean isDone)
			throws IDNotFoundException {
		log.info("execute setItemAsDone(" + iditem + ")");

		try {
			ps = db.connect().prepareStatement(SQLQueries.UPDATE_ITEM_DONE);

			ps.setBoolean(1, isDone);
			ps.setInt(2, iditem);

			if (ps.executeUpdate() < 1) {
				ps.close();
				db.close();

				throw new IDNotFoundException("Object not found error",
						"No item found with id: " + iditem
								+ "! Shopping List could not be modified!",
						40410);
			}

			ps.close();
			db.close();
		} catch (SQLException e) {
			log.error(e.getMessage());
		}
	}

}
