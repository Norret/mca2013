package at.ssl.dao;

import java.sql.Connection;

public interface IDBConnector {
	
	public Connection connect();
	
	public void close();

}
