package at.ssl.dao;

import at.ssl.exception.DuplicateObjectException;
import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.InviteResource;
import at.ssl.resource.PasswordResource;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.UserCollection;
import at.ssl.resource.UserResource;

public interface IUserDAO {

	public UserResource createUser(UserResource u) throws DuplicateObjectException;

	public void deleteUser(int iduser) throws IDNotFoundException;

	public void modifyUser(UserResource u) throws IDNotFoundException, DuplicateObjectException;

	public UserCollection getAllUsers();

	public UserResource getUserById(int iduser) throws IDNotFoundException;

	public void addInvitedUser(InviteResource i) throws DuplicateObjectException;

	public void changePassword(PasswordResource p) throws IDNotFoundException;
	
	public String getPasswordByUser(int iduser) throws IDNotFoundException;

	public void subscribeUserToShoppingList(int iduser, int idShoppingList) throws IDNotFoundException;
	
	public boolean isUserSubscribedToShoppingList(int iduser, int idShoppingList) throws IDNotFoundException;
	
	public void unsubscribeUserFromShoppingList(int iduser, int idShoppingList) throws IDNotFoundException;
	
	public ShoppingListCollection getShoppingListByUser(int iduser) throws IDNotFoundException;
	
	public UserResource getUserByEmail(String eMail) throws IDNotFoundException;
}
