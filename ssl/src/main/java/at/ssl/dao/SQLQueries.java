package at.ssl.dao;

public interface SQLQueries {

	/**
	 * User Queries
	 */

	public static final String SELECT_ALL_USERS = "SELECT * FROM user";

	public static final String SELECT_USER_BY_ID = "SELECT * FROM user WHERE iduser = ? LIMIT 1";

	public static final String DELETE_USER_BY_ID = "DELETE FROM user WHERE iduser = ?";

	public static final String CREATE_USER = "INSERT INTO user (mail, username, avatar, password, activated) VALUES (?, ?, ?, ?, ?)";

	public static final String CREATE_INVITE_USER = "INSERT INTO user (mail, username) VALUES (?, ?)";

	public static final String UPDATE_USER = "UPDATE user SET mail=?, username=?, avatar=? WHERE iduser=?";

	public static final String UPDATE_USER_PASSWORD = "UPDATE user SET password = ? WHERE iduser = ?";

	public static final String SELECT_GET_PASSWORD_BY_USER = "SELECT password FROM user WHERE iduser = ?";

	public static final String INSERT_SUBSCRIBE_SHOPPING_LIST = "INSERT INTO user_has_shopping_list (user_iduser, shopping_list_idshopping_list) VALUES (?, ?)";

	public static final String SELECT_USER_IS_SUBSCRIBED = "SELECT CASE WHEN EXISTS ( SELECT * FROM user_has_shopping_list WHERE user_iduser = ? AND shopping_list_idshopping_list = ?) THEN true ELSE false END AS is_subscribed";

	public static final String DELETE_SUBSCRIBED_USER = "DELETE FROM user_has_shopping_list WHERE user_iduser = ? AND shopping_list_idshopping_list=?";
	
	public static final String SELECT_SHOPPING_LISTS_BY_USER = "SELECT * FROM shopping_list s JOIN user_has_shopping_list us ON us.shopping_list_idshopping_list = s.idshopping_list WHERE us.user_iduser = ?";
	
	public static final String SELECT_USER_BY_EMAIL = "SELECT * FROM user WHERE mail = ? LIMIT 1";

	/**
	 * Shopping List Queries
	 */

	public static final String SELECT_ALL_SHOPPING_LISTS = "SELECT * FROM shopping_list";

	public static final String SELECT_SHOPPING_LIST_BY_ID = "SELECT * FROM shopping_list WHERE idshopping_list = ?";

	public static final String DELETE_SHOPPING_LIST_BY_ID = "DELETE FROM shopping_list WHERE idshopping_list = ?";

	public static final String CREATE_SHOPPING_LIST = "INSERT INTO shopping_list (name, picture, comment, owner, created, updated) VALUES (?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";

	public static final String UPDATE_SHOPPING_LIST = "UPDATE shopping_list SET name=?, picture=?, comment=?, owner=?, updated=CURRENT_TIMESTAMP WHERE idshopping_list=?";
	
	public static final String SELECT_OWNED_SHOPPINGLISTS_BY_USER_ID = "SELECT * FROM shopping_list WHERE owner = ?";
	/**
	 * Item Queries
	 */
	
	public static final String CREATE_ITEM = "INSERT INTO item (picture, price, name, comment, priority, shopping_list_idshopping_list, owner, created, updated, done) VALUES (?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, NOW(), ?)";
	
	public static final String SELECT_ALL_ITEMS_OF_LIST = "SELECT * FROM item WHERE shopping_list_idshopping_list = ?";
	
	public static final String UPDATE_ITEM = "UPDATE item SET picture=?, price=?, name=?, comment=?, priority=?, shopping_list_idshopping_list=?, owner=?, updated=CURRENT_TIMESTAMP, done=? WHERE iditem = ?";
	
	public static final String DELETE_ITEM = "DELETE FROM item WHERE iditem = ?";
	
	public static final String SELECT_ITEM_BY_ID = "SELECT * FROM item WHERE iditem = ?";
	
	public static final String UPDATE_ITEM_DONE = "UPDATE item SET done=? WHERE iditem = ?";
}
