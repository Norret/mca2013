package at.ssl.resource;

public class ShoppingListResource {
	private int idShoppingList;
	private String name;
	private String picture;
	private String comment;
	private int owner;
	private long created;
	private long updated;

	public ShoppingListResource() {

	}

	public ShoppingListResource(int idShoppingList, String name,
			String picture, String comment, int owner) {
		super();
		this.idShoppingList = idShoppingList;
		this.name = name;
		this.picture = picture;
		this.comment = comment;
		this.owner = owner;
	}

	public ShoppingListResource(int idShoppingList, String name,
			String picture, String comment, int owner, long created,
			long updated) {
		this.idShoppingList = idShoppingList;
		this.name = name;
		this.picture = picture;
		this.comment = comment;
		this.owner = owner;
		this.created = created;
		this.updated = updated;
	}
	

	public int getIdShoppingList() {
		return idShoppingList;
	}

	public void setIdShoppingList(int idShoppingList) {
		this.idShoppingList = idShoppingList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public long getUpdated() {
		return updated;
	}

	public void setUpdated(long updated) {
		this.updated = updated;
	}

}
