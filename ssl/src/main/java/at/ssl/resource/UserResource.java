package at.ssl.resource;

public class UserResource {

	private int iduser;
	private String username;
	private String mail;
	private boolean activated;
	private String password;
	private String avatar;
	
	/**
	 * Empty Constructor of User
	 */
	public UserResource() {
		
	}
	
	/**
	 * Constructor of User
	 * 
	 * @param iduser <int>
	 * @param username <String>
	 * @param mail <String>
	 * @param activated <boolean>
	 * @param password <String>
	 * @param avatar <String>
	 */
	public UserResource(int iduser, String username, String mail, boolean activated, String password, String avatar) {
		this.iduser = iduser;
		this.username = username;
		this.mail = mail;
		this.activated = activated;
		this.password = password;
		this.avatar = avatar;
		
	}

	public int getIduser() {
		return iduser;
	}

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
}
