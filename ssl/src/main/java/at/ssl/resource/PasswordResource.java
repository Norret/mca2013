package at.ssl.resource;

public class PasswordResource {
	private int iduser;
	private String oldPassword;
	private String newPassword;
	
	public PasswordResource(int iduser, String oldPassword, String newPassword) {
		this.iduser = iduser;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}

	public int getIduser() {
		return iduser;
	}

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	
}
