package at.ssl.resource;

import java.util.ArrayList;
import java.util.Iterator;

public class ShoppingListCollection {

	private ArrayList<ShoppingListResource> shoppingListCollection;

	public ShoppingListCollection() {
		if (shoppingListCollection == null) {
			shoppingListCollection = new ArrayList<ShoppingListResource>();
		}
	}

	public ArrayList<ShoppingListResource> getShoppingListCollection() {
		return shoppingListCollection;
	}

	public void setShoppingListCollection(
			ArrayList<ShoppingListResource> shoppingListCollection) {
		this.shoppingListCollection = shoppingListCollection;
	}

	public void addShoppingListResource(ShoppingListResource sl) {
		Iterator<ShoppingListResource> itr = shoppingListCollection.iterator();
		boolean containsList = false;

		while (itr.hasNext()) {
			if (itr.next().getIdShoppingList() == sl.getIdShoppingList())
				containsList = true;
		}

		if (!containsList)
			shoppingListCollection.add(sl);
	}

	public void deleteShoppingListResource(int idShoppingList) {
		Iterator<ShoppingListResource> itr = shoppingListCollection.iterator();
		ShoppingListResource sl;

		while (itr.hasNext()) {
			sl = itr.next();

			if (sl.getIdShoppingList() == idShoppingList)
				shoppingListCollection.remove(sl);
		}
	}

	public int size() {
		return this.shoppingListCollection.size();
	}
}
