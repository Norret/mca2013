package at.ssl.resource;

import at.ssl.exception.SSLException;

public class ExceptionResource {

	private String errorName;
	private String errorMessage;
	private int errorCode;
	
	public ExceptionResource() {	
	}
	
	public ExceptionResource(SSLException e) {
		this.errorCode = e.getErrorCode();
		this.errorMessage = e.getErrorMessage();
		this.errorName = e.getErrorName();
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
}
