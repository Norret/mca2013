package at.ssl.resource;

public class InviteResource {

	private int inviterId;
	private String inviteeMail;
	private String inviteeUsername;
	
	public InviteResource() {
		
	}
	
	public InviteResource(int inviterId, String inviteeMail, String inviteeUsername) {
		this.inviterId = inviterId;
		this.inviteeMail = inviteeMail;
		this.inviteeUsername = inviteeUsername;
	}

	public int getInviterId() {
		return inviterId;
	}

	public void setInviterId(int inviterId) {
		this.inviterId = inviterId;
	}

	public String getInviteeMail() {
		return inviteeMail;
	}

	public void setInviteeMail(String inviteeMail) {
		this.inviteeMail = inviteeMail;
	}

	public String getInviteeUsername() {
		return inviteeUsername;
	}

	public void setInviteeUsername(String inviteeUsername) {
		this.inviteeUsername = inviteeUsername;
	}
	
	
	
	
}
