package at.ssl.resource;

public class ItemResource {
	private int idItem;
	private String picture;
	private double price;
	private String name;
	private String comment;
	private int priority;
	private int shopping_list_idshopping_list;
	private int owner;
	private long created;
	private long updated;
	private boolean done;

	public ItemResource() {

	}

	public ItemResource(String picture, int price, String name, String comment,
			int priority, int owner, boolean done) {
		this.picture = picture;
		this.price = price;
		this.name = name;
		this.comment = comment;
		this.priority = priority;
		this.owner = owner;
		this.done = done;
	}

	public ItemResource(int idItem, String picture, int price,
			String name, String comment, int priority,
			int shopping_list_idshopping_list, int owner, long created,
			long updated, boolean done) {
		this.idItem = idItem;
		this.picture = picture;
		this.price = price;
		this.name = name;
		this.comment = comment;
		this.priority = priority;
		this.shopping_list_idshopping_list = shopping_list_idshopping_list;
		this.owner = owner;
		this.created = created;
		this.updated = updated;
		this.done = done;
	}

	public int getIdItem() {
		return idItem;
	}

	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double d) {
		this.price = d;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getShopping_list_idshopping_list() {
		return shopping_list_idshopping_list;
	}

	public void setShopping_list_idshopping_list(
			int shopping_list_idshopping_list) {
		this.shopping_list_idshopping_list = shopping_list_idshopping_list;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public long getUpdated() {
		return updated;
	}

	public void setUpdated(long updated) {
		this.updated = updated;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

}
