package at.ssl.resource;

import java.util.ArrayList;
import java.util.Iterator;

public class UserCollection {
 
	private ArrayList<UserResource> userCollection;

	public UserCollection() {
		if(userCollection == null)
			userCollection = new ArrayList<UserResource>();
	}
	
	public ArrayList<UserResource> getUserCollection() {
		return userCollection;
	}

	public void setUserCollection(ArrayList<UserResource> userCollection) {
		this.userCollection = userCollection;
	}
	
	public void addUserResource(UserResource user) {
		Iterator<UserResource> itr = userCollection.iterator();
		boolean containsUser = false;
		while(itr.hasNext()) {
			if(itr.next().getIduser() == user.getIduser())
				containsUser = true;
		}
		
		if(!containsUser)
			userCollection.add(user);
	}
	
	public void deleteUserResource(int iduser) {
		Iterator<UserResource> itr = userCollection.iterator();
		UserResource user;
		while(itr.hasNext()) {
			user = itr.next();
			
			if(user.getIduser() == iduser)
				userCollection.remove(user);
		}
	}
	
	public int size() {
		return this.userCollection.size();
	}
}
