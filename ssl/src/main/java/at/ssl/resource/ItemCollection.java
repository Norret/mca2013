package at.ssl.resource;

import java.util.ArrayList;
import java.util.Iterator;

public class ItemCollection {

	private ArrayList<ItemResource> itemCollection;

	public ItemCollection() {
		if (itemCollection == null) {
			itemCollection = new ArrayList<ItemResource>();
		}
	}

	public ArrayList<ItemResource> getItemCollection() {
		return itemCollection;
	}

	public void setItemCollection(ArrayList<ItemResource> iCollection) {
		this.itemCollection = iCollection;
	}

	public void addItemResource(ItemResource i) {
		Iterator<ItemResource> itr = itemCollection.iterator();
		boolean containsItem = false;

		while (itr.hasNext()) {

			if (itr.next().getIdItem() == i.getIdItem())
				containsItem = true;
		}

		if (!containsItem)
			itemCollection.add(i);
	}

	public void deleteItemResource(int idItem) {
		Iterator<ItemResource> itr = itemCollection.iterator();
		ItemResource i;

		while (itr.hasNext()) {
			i = itr.next();

			if (i.getIdItem() == idItem)
				itemCollection.remove(i);
		}
	}

	public int size() {
		return this.itemCollection.size();
	}
}
