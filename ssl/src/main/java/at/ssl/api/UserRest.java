package at.ssl.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.ssl.exception.DuplicateObjectException;
import at.ssl.exception.IDNotFoundException;
import at.ssl.exception.WrongLoginException;
import at.ssl.helper.RestSupport;
import at.ssl.resource.ExceptionResource;
import at.ssl.resource.InviteResource;
import at.ssl.resource.LoginResource;
import at.ssl.resource.PasswordResource;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.UserCollection;
import at.ssl.resource.UserResource;
import at.ssl.service.IShoppingListService;
import at.ssl.service.ShoppingListService;
import at.ssl.service.UserService;

@Path("/users")
@Produces({ "application/json" })
public class UserRest {
	private static UserService userService;
	private static IShoppingListService slService;

	public UserRest() {
		if (userService == null)
			userService = new UserService();
		if (slService == null)
			slService = new ShoppingListService();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("id") int iduser) {

		UserResource user;
		try {
			user = userService.getUserById(iduser);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}

		return RestSupport.createResponse(200, user);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsers(UserResource user) {
		try {
			user = userService.createUser(user);

			return RestSupport.createResponse(201, user);
		} catch (DuplicateObjectException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers() {

		UserCollection uCollection = userService.getAllUsers();

		return RestSupport.createResponse(200, uCollection);
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyUser(@PathParam("id") int iduser, UserResource u) {

		try {
			u = userService.modifyUser(u);

			return RestSupport.createResponse(204, u);

		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		} catch (DuplicateObjectException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));

		}
	}

	@DELETE
	@Path("{id}")
	public Response deleteUser(@PathParam("id") int iduser) {

		try {
			userService.deleteUserById(iduser);

			return RestSupport.createResponse(204);

		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@POST
	@Path("{id}/invite")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response inviteFriend(@PathParam("id") int iduser, InviteResource i) {

		try {
			userService.InviteUser(i);

			return RestSupport.createResponse(201);

		} catch (DuplicateObjectException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@POST
	@Path("{id}/changepassword")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changePassword(@PathParam("id") int iduser,
			PasswordResource p) {

		try {
			userService.changePassword(p);

			return RestSupport.createResponse(204);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@POST
	@Path("{iduser}/shoppinglists/{idshoppinglist}")
	public Response subscribeUserToShoppingList(
			@PathParam("iduser") int iduser,
			@PathParam("idshoppinglist") int idShoppingList) {

		try {
			userService.subscribeUserToShoppingList(iduser, idShoppingList);

			return RestSupport.createResponse(201);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		} catch (DuplicateObjectException e) {
			return RestSupport.createResponse(409, new ExceptionResource(e));
		}
	}

	@DELETE
	@Path("{iduser}/shoppinglists/{idshoppinglist}")
	public Response unsubscribeUserFromShoppingList(
			@PathParam("iduser") int iduser,
			@PathParam("idshoppinglist") int idShoppingList) {

		try {
			userService.unsubscribeUserFromShoppingList(iduser, idShoppingList);

			return RestSupport.createResponse(204);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}

	}

	@GET
	@Path("{iduser}/shoppinglists")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getShoppingListByUser(@PathParam("iduser") int iduser) {

		try {
			ShoppingListCollection sCollection = userService
					.getShoppingListByUser(iduser);

			System.out
					.println("users/" + iduser + "/shoppinglists successfull");

			return RestSupport.createResponse(200, sCollection);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@GET
	@Path("{iduser}/owns")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOwnedShoppingLists(@PathParam("iduser") int iduser) {

		try {
			ShoppingListCollection sCollection = slService
					.getOwnedShoppingLists(iduser);

			return RestSupport.createResponse(200, sCollection);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@PUT
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response performUserLogin(LoginResource l) {
		try {
			UserResource u = userService.performUserLogin(l);

			return RestSupport.createResponse(200, u);

		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		} catch (WrongLoginException e) {
			return RestSupport.createResponse(402, new ExceptionResource(e));
		}
	}

}
