package at.ssl.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.ssl.exception.IDNotFoundException;
import at.ssl.helper.RestSupport;
import at.ssl.resource.ExceptionResource;
import at.ssl.resource.ItemResource;
import at.ssl.service.IItemService;
import at.ssl.service.ItemService;

@Path("/items")
@Produces({ "application/json" })
public class ItemRest {

	private static IItemService itemService;

	public ItemRest() {
		if (itemService == null)
			itemService = new ItemService();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response createItemResource(ItemResource i) {

		try {
			i = itemService.createItemResource(i);

			return RestSupport.createResponse(201, i);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@PUT
	@Path("{iditem}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyItemByID(@PathParam("iditem") int idItem,
			ItemResource i) {

		try {
			i = itemService.modifyItemResource(i);

			return RestSupport.createResponse(201, i);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@DELETE
	@Path("{iditem}")
	public Response deleteItemById(@PathParam("iditem") int idItem) {

		try {
			itemService.deleteItemResource(idItem);

			return RestSupport.createResponse(204);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@PUT
	@Path("{iditem}/done")
	@Produces(MediaType.APPLICATION_JSON)
	public Response toogleItemDone(@PathParam("iditem") int idItem) {

		try {
			ItemResource i = itemService.toogleItemDone(idItem);

			return RestSupport.createResponse(204, i);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

}
