package at.ssl.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.ssl.exception.IDNotFoundException;
import at.ssl.helper.RestSupport;
import at.ssl.resource.ExceptionResource;
import at.ssl.resource.ItemResource;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.ShoppingListElapsedItemsResource;
import at.ssl.resource.ShoppingListResource;
import at.ssl.service.IItemService;
import at.ssl.service.IShoppingListService;
import at.ssl.service.IUserService;
import at.ssl.service.ItemService;
import at.ssl.service.ShoppingListService;
import at.ssl.service.UserService;

@Path("/shoppinglists")
@Produces({ "application/json" })
public class ShoppingListRest {

	private static IShoppingListService shoppingListService;
	private static IUserService userService;
	private static IItemService itemService;
	public ShoppingListRest() {
		if (shoppingListService == null)
			shoppingListService = new ShoppingListService();

		if (userService == null)
			userService = new UserService();
		
		if(itemService == null) 
			itemService = new ItemService();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getShoppingList(@PathParam("id") int idShoppingList) {

		ShoppingListResource sl;

		try {
			sl = shoppingListService.getShoppingListById(idShoppingList);

			return RestSupport.createResponse(200, sl);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createShoppingList(ShoppingListResource sl) {

		try {
			sl = shoppingListService.createShoppingList(sl);

			return RestSupport.createResponse(201, sl);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllShoppingLists() {

		ShoppingListCollection sCollection = shoppingListService
				.getAllShoppingLists();

		return RestSupport.createResponse(200,
				sCollection.getShoppingListCollection());
	}

	@DELETE
	@Path("{id}")
	public Response deleteUserById(@PathParam("id") int idShoppingList) {

		try {
			shoppingListService.deleteShoppingListById(idShoppingList);

			return RestSupport.createResponse(204);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}

	@DELETE
	@Path("{idshoppinglist}/users/{iduser}")
	public Response deleteUserFromShoppingList(@PathParam("iduser") int iduser,
			@PathParam("idshoppinglist") int idShoppingList) {
		try {
			userService.unsubscribeUserFromShoppingList(iduser, idShoppingList);

			return RestSupport.createResponse(204);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}
	
	@GET
	@Path("{idshoppinglist}/items")
	public Response getShoppingListWithItems(@PathParam("idshoppinglist") int idShoppingList) {
		
		try {
			ShoppingListElapsedItemsResource sli = itemService.getShoppingListWithItems(idShoppingList);
			
			return RestSupport.createResponse(200, sli);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}
	
	@POST
	@Path("{idshoppinglist}/items")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addItemToShoppingList(@PathParam("idshoppinglist") int idShoppingList, ItemResource i) {
		
		try {
			itemService.createItemResource(i);
			
			ShoppingListElapsedItemsResource sli = itemService.getShoppingListWithItems(idShoppingList);
			
			return RestSupport.createResponse(201, sli);
		} catch (IDNotFoundException e) {
			return RestSupport.createResponse(404, new ExceptionResource(e));
		}
	}
	
}
