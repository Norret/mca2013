package at.ssl.service;

import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.ShoppingListResource;

public interface IShoppingListService {

	public ShoppingListCollection getAllShoppingLists();

	public ShoppingListResource getShoppingListById(int idShoppingList)
			throws IDNotFoundException;

	public void deleteShoppingListById(int idShoppingList)
			throws IDNotFoundException;

	public ShoppingListResource createShoppingList(ShoppingListResource sl) throws IDNotFoundException;

	public ShoppingListResource modifyShoppingList(ShoppingListResource sl) throws IDNotFoundException;
	
	public ShoppingListCollection getOwnedShoppingLists(int iduser) throws IDNotFoundException;

}
