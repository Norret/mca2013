package at.ssl.service;

import at.ssl.dao.IUserDAO;
import at.ssl.dao.UserDAO;
import at.ssl.exception.DuplicateObjectException;
import at.ssl.exception.IDNotFoundException;
import at.ssl.exception.WrongLoginException;
import at.ssl.resource.InviteResource;
import at.ssl.resource.LoginResource;
import at.ssl.resource.PasswordResource;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.UserCollection;
import at.ssl.resource.UserResource;

public class UserService implements IUserService {
	private IUserDAO userDAO;

	public UserService() {
		userDAO = new UserDAO();
	}

	@Override
	public UserCollection getAllUsers() {
		return userDAO.getAllUsers();
	}

	@Override
	public UserResource getUserById(int iduser) throws IDNotFoundException {
		return userDAO.getUserById(iduser);
	}

	@Override
	public void deleteUserById(int iduser) throws IDNotFoundException {
		userDAO.deleteUser(iduser);

	}

	@Override
	public UserResource createUser(UserResource u)
			throws DuplicateObjectException {
		return userDAO.createUser(u);
	}

	@Override
	public UserResource modifyUser(UserResource u) throws IDNotFoundException,
			DuplicateObjectException {
		userDAO.modifyUser(u);

		return userDAO.getUserById(u.getIduser());
	}

	@Override
	public void InviteUser(InviteResource i) throws DuplicateObjectException {
		userDAO.addInvitedUser(i);

	}

	@Override
	public void changePassword(PasswordResource p) throws IDNotFoundException {
		if (userDAO.getPasswordByUser(p.getIduser()).equals(p.getOldPassword())) {
			userDAO.changePassword(p);
		}
	}

	@Override
	public void subscribeUserToShoppingList(int iduser, int idShoppingList)
			throws IDNotFoundException, DuplicateObjectException {

		if (!userDAO.isUserSubscribedToShoppingList(iduser, idShoppingList))
			userDAO.subscribeUserToShoppingList(iduser, idShoppingList);
		else
			throw new DuplicateObjectException("Object already exists error!",
					"User with id: " + iduser + " and ShoppingList with id: "
							+ idShoppingList + " are already connected", 1);

	}

	@Override
	public void unsubscribeUserFromShoppingList(int iduser, int idShoppingList)
			throws IDNotFoundException {

		userDAO.unsubscribeUserFromShoppingList(iduser, idShoppingList);

	}

	@Override
	public ShoppingListCollection getShoppingListByUser(int iduser)
			throws IDNotFoundException {

		return userDAO.getShoppingListByUser(iduser);

	}

	@Override
	public UserResource performUserLogin(LoginResource l)
			throws IDNotFoundException, WrongLoginException {
		
		UserResource user = userDAO.getUserByEmail(l.getMail());
		
		if(user != null) {
			if(user.getPassword().equals(l.getPassword())) {
				return user;
			} else {
				throw new WrongLoginException("Wrong Login", "Username or password is not correct", 40201);
			}
		}
		
		return null;
		
		
		
	}

}
