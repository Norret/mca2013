package at.ssl.service;

import at.ssl.exception.DuplicateObjectException;
import at.ssl.exception.IDNotFoundException;
import at.ssl.exception.WrongLoginException;
import at.ssl.resource.InviteResource;
import at.ssl.resource.LoginResource;
import at.ssl.resource.PasswordResource;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.UserCollection;
import at.ssl.resource.UserResource;

public interface IUserService {

	public UserCollection getAllUsers();
	
	public UserResource getUserById(int iduser) throws IDNotFoundException;
	
	public void deleteUserById(int iduser) throws IDNotFoundException;
	
	public UserResource createUser(UserResource u) throws DuplicateObjectException;
	
	public UserResource modifyUser(UserResource u) throws IDNotFoundException, DuplicateObjectException;
	
	public void InviteUser(InviteResource i) throws DuplicateObjectException;
	
	public void changePassword(PasswordResource p) throws IDNotFoundException;
	
	public void subscribeUserToShoppingList(int iduser, int idShoppingList) throws IDNotFoundException, DuplicateObjectException;
	
	public void unsubscribeUserFromShoppingList(int iduser, int idShoppingList) throws IDNotFoundException;
	
	public ShoppingListCollection getShoppingListByUser(int iduser) throws IDNotFoundException;
	
	public UserResource performUserLogin(LoginResource u) throws IDNotFoundException, WrongLoginException;
}
