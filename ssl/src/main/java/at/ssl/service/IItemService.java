package at.ssl.service;

import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ItemResource;
import at.ssl.resource.ShoppingListElapsedItemsResource;

public interface IItemService {

	public ItemResource createItemResource(ItemResource i) throws IDNotFoundException;
	
	public ShoppingListElapsedItemsResource getShoppingListWithItems(int idShoppingList) throws IDNotFoundException;
	
	public ItemResource modifyItemResource(ItemResource i) throws IDNotFoundException;
	
	public void deleteItemResource(int idItem) throws IDNotFoundException;

	ItemResource toogleItemDone(int id) throws IDNotFoundException;
}
