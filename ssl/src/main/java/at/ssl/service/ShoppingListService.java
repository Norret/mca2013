package at.ssl.service;

import at.ssl.dao.IShoppingListDAO;
import at.ssl.dao.IUserDAO;
import at.ssl.dao.ShoppingListDAO;
import at.ssl.dao.UserDAO;
import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ShoppingListCollection;
import at.ssl.resource.ShoppingListResource;

public class ShoppingListService implements IShoppingListService {

	private IShoppingListDAO shoppingListDAO;
	private IUserDAO userDAO;

	public ShoppingListService() {
		shoppingListDAO = new ShoppingListDAO();
		userDAO = new UserDAO();
	}

	@Override
	public ShoppingListCollection getAllShoppingLists() {
		return shoppingListDAO.getAllShoppingLists();
	}

	@Override
	public ShoppingListResource getShoppingListById(int idShoppingList)
			throws IDNotFoundException {
		return shoppingListDAO.getShoppingListById(idShoppingList);
	}

	@Override
	public void deleteShoppingListById(int idShoppingList)
			throws IDNotFoundException {
		shoppingListDAO.deleteShoppingList(idShoppingList);

	}

	@Override
	public ShoppingListResource createShoppingList(ShoppingListResource sl)
			throws IDNotFoundException {
		sl = shoppingListDAO.createShoppingList(sl);
		userDAO.subscribeUserToShoppingList(sl.getOwner(),
				sl.getIdShoppingList());

		return sl;

	}

	@Override
	public ShoppingListResource modifyShoppingList(ShoppingListResource sl)
			throws IDNotFoundException {
		shoppingListDAO.modifyShoppingList(sl);

		return shoppingListDAO.getShoppingListById(sl.getIdShoppingList());

	}

	@Override
	public ShoppingListCollection getOwnedShoppingLists(int iduser)
			throws IDNotFoundException {
		return shoppingListDAO.getOwnedShoppingLists(iduser);
	}

}
