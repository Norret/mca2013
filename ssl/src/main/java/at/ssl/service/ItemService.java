package at.ssl.service;

import at.ssl.dao.IItemDAO;
import at.ssl.dao.ItemDAO;
import at.ssl.exception.IDNotFoundException;
import at.ssl.resource.ItemCollection;
import at.ssl.resource.ItemResource;
import at.ssl.resource.ShoppingListElapsedItemsResource;
import at.ssl.resource.ShoppingListResource;

public class ItemService implements IItemService {

	private IItemDAO itemDAO;
	private IShoppingListService slService;

	public ItemService() {
		itemDAO = new ItemDAO();
		slService = new ShoppingListService();
	}

	@Override
	public ItemResource createItemResource(ItemResource i)
			throws IDNotFoundException {
		itemDAO.createItemResource(i);

		return itemDAO.getItemById(i.getIdItem());
	}

	@Override
	public ShoppingListElapsedItemsResource getShoppingListWithItems(
			int idShoppingList) throws IDNotFoundException {

		ShoppingListResource sl = slService.getShoppingListById(idShoppingList);

		ShoppingListElapsedItemsResource slext;
		if (sl != null) {
			slext = new ShoppingListElapsedItemsResource(sl);

			ItemCollection iCollection = itemDAO
					.getAllItemsOfShoppingList(idShoppingList);
			if (iCollection != null) {
				if (iCollection.size() > 0)
					slext.setItemCollection(iCollection.getItemCollection());
				return slext;
			}

		}

		return null;

	}

	@Override
	public ItemResource modifyItemResource(ItemResource i)
			throws IDNotFoundException {

		itemDAO.modifyItemResource(i);

		return itemDAO.getItemById(i.getIdItem());
	}

	@Override
	public void deleteItemResource(int idItem) throws IDNotFoundException {
		itemDAO.deleteItemResource(idItem);
	}

	@Override
	public ItemResource toogleItemDone(int idItem) throws IDNotFoundException {
		ItemResource i = itemDAO.getItemById(idItem);
		
		if(i != null) {
			i.setDone(!i.isDone());
		} else {
			throw new IDNotFoundException("Item not Found", "Item with id: "+idItem+" was not found", 40450);
		}
		

		itemDAO.setItemAsDone(idItem, i.isDone());

		return i;
	}

}
