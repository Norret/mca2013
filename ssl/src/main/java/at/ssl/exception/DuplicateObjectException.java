package at.ssl.exception;

public class DuplicateObjectException extends SSLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7391833019526293925L;
	
	public DuplicateObjectException(String errorName, String errorMessage, int errorCode) {
		super(errorName, errorMessage, errorCode);
	}

}
