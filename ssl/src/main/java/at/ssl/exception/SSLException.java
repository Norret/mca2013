package at.ssl.exception;

public abstract class SSLException extends Exception {

	private String errorName;
	private String errorMessage;
	private int errorCode;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SSLException() {
		super();
	}
	
	public SSLException(String message) {
		super(message);
	}
	
	public SSLException(String errorName, String errorMessage, int errorCode) {
		super(errorMessage);
		
		this.errorName = errorName;
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
