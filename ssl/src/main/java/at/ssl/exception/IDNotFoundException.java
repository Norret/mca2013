package at.ssl.exception;

public class IDNotFoundException extends SSLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 700548217860441442L;

	public IDNotFoundException(String errorName, String errorMessage, int errorCode) {
		super(errorName, errorMessage, errorCode);
	}
	
	

}
