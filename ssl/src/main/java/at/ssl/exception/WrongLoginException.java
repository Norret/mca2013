package at.ssl.exception;

public class WrongLoginException extends SSLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8894847511860471218L;

	public WrongLoginException(String errorName, String errorMessage, int errorCode) {
		super(errorName, errorMessage, errorCode);
	}
}
