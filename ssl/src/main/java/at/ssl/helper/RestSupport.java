package at.ssl.helper;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;

public class RestSupport {

	public static Response createResponse(int status, Object object) {
		return Response.status(status).entity(new Gson().toJson(object)).build();
	}
	
	public static Response createResponse(int status) {
		return Response.status(status).build();
	}
}
