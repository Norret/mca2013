<!DOCTYPE html>
<html>
  <head>
    <title>Welcome to Shared Shopping List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <script type="text/javascript">
    	function handleSubmit(e) {
    		if($("#user").val() === "" || $("#pdw").val() === "") {
    			$("#alertbox").show();
    		} else {
    			window.location.href = "internal/home.jsp";
    			event.preventDefault(); 
    			return false;
    		}
    	}
    </script>
  
  	<div class="alert alert-success" style="visibility:hidden" id="alertbox">Please fill out the whole form!</div>
  
    <div class="container">
	
      <form class="form-signin" role="form" onSubmit="JavaScript:handleSubmit()">
      	<h1>Welcome to Shared Shopping List Webapplication</h1>
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" id="user" class="form-control" placeholder="Email address" required autofocus>
        <input type="password" id="pwd" class="form-control" placeholder="Password" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        
        <div style="height:30px; padding-top:30px">
        	<label>If you are not registered yet, please click here:</label>
        	<a href="registration/registration.jsp" target="_self">Registration</a>
        </div>
      </form>

    </div>
    
  </body>
</html>
