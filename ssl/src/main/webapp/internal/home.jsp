<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Shared Shopping List</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css"
	rel="stylesheet" media="screen">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">
<script src="https://code.jquery.com/jquery.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<script type="text/javascript">
	function handleCreateShoppingList() {
		
		var data= {
			"name": $("#shoppinglistname").val(), 
			"owner": "1", 
			"comment": $("#shoppinglistcomment").val(),
			"picture": $("#picture").val()
		};
		
		$.ajax({
			type:"POST",
			contentType:"application/json",
			url: "/SharedShoppingList/rest/shoppinglists",
			data: JSON.stringify(data),
			cache: false, 
			async: false
		}).done(function(ret){
			
		});
	};
	

	
</script>

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Shared Shopping List</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class=""><a id="home" href="#">Home</a></li>
					<li><a id="usercontrol" href="#">Usercontrol</a></li>
					<li><a id="createshoppinglist" href="#">Create
							Shoppinglist</a></li>
					<li><a id="myshoppinglists" href="#">My Shoppinglists</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">About</a></li>
					<li><a href="#">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="starter-template" id="homeDiv">
			<div style="height: 48px"></div>
			<h1>Welcome to Shared Shopping List</h1>
			<p class="lead">This is your home screen. Please select a submenu
				from above</p>
		</div>

		<div class="starter-template" id="usercontrolDiv"
			style="display: none">
			<div style="height: 48px"></div>
			<h1>Usercontrol</h1>
			<p class="lead">This is your home screen. Please select a submenu
				from above</p>
		</div>

		<div class="starter-template" id="createshoppinglistDiv"
			style="display: none">
			<div style="height: 48px"></div>
			<h1>Create new shopping list</h1>
			<p class="lead">Please insert the following fields:</p>
			<form class="form-signin" role="form"
				onSubmit="JavaScript:handleCreateShoppingList()">
				<div class="form-group">
					<label for="shoppinglistname">shoppinglistname: </label> <input
						id="shoppinglistname" type="text" class="form-control"
						placeholder="Name" required autofocus>
				</div>
				<div class="form-group">
					<label for="shoppinglistcomment">Comment: </label> <input
						id="shoppinglistcomment" type="comment" class="form-control"
						placeholder="comment" required>
				</div>
				<div class="form-group">
					<label for="picture">Picture: </label> <input id="picture"
						type="comment" class="form-control" placeholder="picture url"
						required>
				</div>

				<button id="submitbutton" class="btn btn-lg btn-primary "
					type="submit">Create Shoppinglist</button>
			</form>
		</div>

		<div class="starter-template" id="myshoppinglistsDiv"
			style="display: none">
			<div style="height: 48px"></div>
			<h1>My shoppinglists</h1>
			<p class="lead">Here are your current shoppinglists</p>
			<div id="shoppingListPlaceHolder"></div>
		</div>

	</div>

	<script type="text/javascript">
    	(function(){
    		
			// create some dummy data in the model
			//Client.model.ssl = {}; 
			//var model = Client.model.ssl;
			
			//model.username = "juergen.hirsch@gmail.com";
			
			
			
			// Add click events for Button items 	
			
			function toggleDivs(divName) {
				var currentDivs = ["usercontrolDiv", "homeDiv", "createshoppinglistDiv", "myshoppinglistsDiv"];
				
				for(var div in currentDivs) {
					$("#"+currentDivs[div]).hide(); 
					
					$("#"+divName).show();
				}
			}; 
			
			$("a#home").click(function(){
				toggleDivs("homeDiv");
			});
			
			$("a#usercontrol").click(function(){
				toggleDivs("usercontrolDiv");
			});
			
			$("a#createshoppinglist").click(function(){
				toggleDivs("createshoppinglistDiv");
			});
			
			$("a#myshoppinglists").click(function(){
				
				var placeHolder = $("#shoppingListPlaceHolder"); 
				$(placeHolder).empty(); 
				
				
				
				$.ajax({
					type:"GET",
					url: "/SharedShoppingList/rest/users/1/shoppinglists",
					cache: false, 
					async: false
				}).done(function(ret){
					if(ret.errorMessage){
						alert(ret.errorMessage);
					} else {
						
						$.each( ret.shoppingListCollection, function( i, obj ) {
							$.each( obj, function( key, value ) {
								if(key == "name") {
									
									$(placeHolder).append("<p><div class=\"row\"><div width=\"200\" class=\".col-md-4\">"+value+"&nbsp;&nbsp;</div><div class=\".col-md-8\"><button type='button' class='btn btn-primary'>See content</button> <button type='button' class='btn btn-danger'>Delete</button>"+"</div></div></p>");
								}  
								
							});
						});
						

					}
				});
				
				toggleDivs("myshoppinglistsDiv");
			});
			
			
    	})();
</script>
</body>
</html>