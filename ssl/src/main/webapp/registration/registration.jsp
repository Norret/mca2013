<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<script type="text/javascript">
    	function handleSubmit(e) {
    		alert("Die Registrierung war erfolgreich");
    	}
    </script>

	<div class="container">
		<form class="form-signin" role="form" onSubmit="JavaScript:handleSubmit()">
      	<h1>Registration form</h1>
        <h2 class="form-signin-heading">Please fill out the following information</h2>
        <div class="form-group">
        	<label for="username">Username: </label>
        	<input id="username" type="text" class="form-control" placeholder="Email address" required autofocus>     	
        </div>
        <div class="form-group">
        	<label for="password">Password: </label>
        	<input id="password" type="password" class="form-control" placeholder="Password" required>     	
        </div>
        <div class="form-group">
        	<label for="birthdate">Birthdate: </label>
        	<input id="birthdate" type="text" class="form-control" placeholder="01.01.1970" required>     	
        </div>
        <div class="form-group">
        	<label for="address">Address: </label>
        	<input type="text" class="form-control" placeholder="742 Evergreen Terrace, Springfield" required>     	
        </div>
        
        <button id="submitbutton" class="btn btn-lg btn-primary btn-block" type="submit">Submit Registration</button>
        </form>
    </div> 
</body>
</html>